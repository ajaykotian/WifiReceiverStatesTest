package com.ajay.wifireceiverstatestest;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final int PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION = 143;
    private static MainActivity context;
    private TextView tvLogs;
    private Button btnClearLogs;
    private WifiManager wifiManager;
    private final BroadcastReceiver mWifiScanReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context c, Intent intent) {
            Log.d("BroadCastReceiver", "onReceive: wifi receiver called");

            Log.d("BroadCastReceiver", "onReceive: scan receiver unregisterd");

            getWifiScanResults();
            c.unregisterReceiver(mWifiScanReceiver);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        tvLogs = (TextView) findViewById(R.id.tv_log_display);
        btnClearLogs = (Button) findViewById(R.id.btn_clear_logs);
        wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        btnClearLogs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvLogs.setText("");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_scan:

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                            PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION);
                    //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method

                } else {
                    startScanForResults();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // Do something with granted permission
            startScanForResults();
        }
    }

    public void startScanForResults()
    {
        registerReceiver(mWifiScanReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        wifiManager.startScan();
    }

    public void getWifiScanResults() {

        String result = "";
        List<ScanResult> scanResults = wifiManager.getScanResults();
        for (int i = 0; i < scanResults.size(); i++) {
            result += "\n\nBSSID:    " + scanResults.get(i).BSSID + "\n";
            result += "\nSSID:    " + scanResults.get(i).SSID + "\n\n";
        }
        tvLogs.setText(result);
    }

    public static Context getMainActivityContext() {
        return context;
    }


    public TextView getTextViewInstance() {
        return tvLogs;
    }
}
