package com.ajay.wifireceiverstatestest;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiManager;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.widget.TextView;

import java.util.Objects;


/**
 * Created by Shraddha on 26/8/16.
 */
public class WifiReceiver extends WakefulBroadcastReceiver {

    private static final String TAG = WifiReceiver.class.getSimpleName();


    @Override
    public void onReceive(Context context, Intent intent) {
        SupplicantState state;
        String log = "\n\n\n---------------------onReceive Start-----------------------\n\n";

        MainActivity mainActivity  = (MainActivity) MainActivity.getMainActivityContext();
        Log.e("Wifi Action", intent.getAction());
        log+=("Wifi Action  "+intent.getAction()+"  \n");
        TextView tvLog;


        if (intent.getAction().equals(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION)) {
            state = intent.getParcelableExtra(WifiManager.EXTRA_NEW_STATE);
            log+=( "Supplicant action: " + state.toString()+"  \n");

            Log.e("----------------------", "----------------------");
            Log.e("----------------------", "----------------------");
            Log.e( "Supplicant action: " , state.toString());
            Log.e(TAG, "Supplicant action: " + state.toString());
            switch (state) {
                case COMPLETED:
                    Log.e(TAG, "COMPLETED");
                    break;
                case DISCONNECTED:
                    Log.e(TAG, "DISCONNECTED");
                    mainActivity.startScanForResults();
                    break;
                case SCANNING:
                    Log.e(TAG, "SCANNING");
                    break;
            }
        }


        int extraWifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, 12);

        Log.e("----------------------", "----------------------");
        Log.e("----------------------", "----------------------");
        Log.e("Wifi State",extraWifiState+" - "+resolveWifiState(extraWifiState));
        log+=("Wifi State "+extraWifiState+" - "+resolveWifiState(extraWifiState));


        log+="\n\n----------------------onReceive End----------------------";
        if(mainActivity!=null)
        {
            tvLog = mainActivity.getTextViewInstance();
            if(tvLog!=null)
            {
                tvLog.append(log);
            }
        }
    }

    public String resolveWifiState(int wifiState) {
        String state = "";
        switch (wifiState) {
            case WifiManager.WIFI_STATE_DISABLED:
                state += "WIFI_STATE_DISABLED";
                break;
            case WifiManager.WIFI_STATE_DISABLING:
                state += "WIFI_STATE_DISABLING";
                break;
            case WifiManager.WIFI_STATE_ENABLING:
                state += "WIFI_STATE_ENABLING";
                break;
            case WifiManager.WIFI_STATE_ENABLED:
                state += "WIFI_STATE_ENABLED";
                break;
            case WifiManager.WIFI_STATE_UNKNOWN:
                state += "WIFI_STATE_UNKNOWN";
                break;
        }

        return state;
    }

}
